import csv


class MqttPublish:
    @staticmethod
    def mqtt_publish(client):
        def on_message(client, userdata, message):
            print("message received ", str(message.payload.decode("utf-8")))
            print("message topic=", message.topic)
            print("message qos=", message.qos)
            print("message retain flag=", message.retain)

        input1 = int(input("enter choice 1:kwh 2:kvah 3:kw 4:kva 5:current"))
        client.on_message = on_message
        kwh = []
        kvah = []
        kw = []
        kva = []
        current = []
        if input1 == 1:
            client.subscribe("Kwh")
        if input1 == 2:
            client.subscribe("kVAh")
        if input1 == 3:
            client.subscribe("kW")
        elif input1 == 4:
            client.subscribe("kVA")
        elif input1 == 5:
            client.subscribe("current")
        with open('E:\\input.csv', mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                # print(row)
                client.loop_start()
                kwh.append(str({"Timestamp": row["Timestamp"], "Value": row['kWh']}))
                kvah.append(str({"Timestamp": row["Timestamp"], "Value": row['kVAh']}))
                kw.append(str({"Timestamp": row["Timestamp"], "Value": row['kW']}))
                kva.append(str({"Timestamp": row["Timestamp"], "Value": row['kVA']}))
                current.append(str({"Timestamp": row["Timestamp"], "Value": row['current']}))
                # client.loop_stop()

            client.publish("Kwh", str({"kwh": kwh}))
            client.publish("kVAh", str({"kvah": kvah}))
            client.publish("kW", str({"kw": kw}))
            client.publish("kVA", str({"kva": kva}))
            client.publish("current", str({"current": current}))
