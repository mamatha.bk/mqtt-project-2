import logging
import time
from Scripts.core.mqtt_publish_subscribe1 import MqttPublish

import paho.mqtt.client as mqtt

mqtt_publish_object = MqttPublish()


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("connected OK Returned code")
    else:
        print("Bad connection Returned code")


broker_address = "192.168.0.220"
port = 1883
client = mqtt.Client("P21")
try:
    client.on_connect = on_connect
    print("connecting to broker to ", broker_address)
    client.connect(broker_address, port)
    mqtt_publish_object.mqtt_publish(client)
    client.loop_start()
    time.sleep(4)
    client.loop_stop()

except Exception as e:
    logging.exception("Exception occurred in connection", exc_info=True)

finally:
    client.disconnect()
